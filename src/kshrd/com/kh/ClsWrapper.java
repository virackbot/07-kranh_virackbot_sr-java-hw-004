package kshrd.com.kh;
public class ClsWrapper<T>{
    private Object[] list;
    private int count;
    private int size;
    public ClsWrapper() {
        list = new Object[1];
        count = 0;
        size = 1;
    }
    public void addItem(T item) throws ClsCustomize {

        if(count == size){
            sizeIncrease();
        }

        // Null exception
        if(item == null)
            throw new ClsCustomize("\t\n >> Your input is Null");

        // Duplicate exception
        for(int i=0; i <count; i++){

            if(list[i] == item){
                throw new ClsCustomize("\t\n >>Your Input is Duplicated : "+count);
            }
        }
        list[count] = item;
        count++;
    }
    public Object getItem(int index) {
        return list[index];
    }
    public int size(){
        return count;
    }
    public void sizeIncrease()
    {
        Object[] temp = null;
        if (count == size) {
            temp = new Object[size * 3];
            {
                if (size >= 0) System.arraycopy(list, 0, temp, 0, size);
            }
        }
        list = temp;
        size = size * 3;
    }
}
